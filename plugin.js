// Function to remove clarify boxes
function removeClarifyBoxes() {
  const clarifyBoxes = document.querySelectorAll('div#clarify-box');
  clarifyBoxes.forEach(function (clarifyBox) {
    clarifyBox.remove();
  });
}

// Create a MutationObserver to detect changes in the DOM
const observer = new MutationObserver(function (mutationsList) {
  for (let mutation of mutationsList) {
    if (mutation.type === 'childList' && mutation.addedNodes.length > 0) {
      removeClarifyBoxes();
    }
  }
});

// Start observing changes in the document body
observer.observe(document.body, { childList: true, subtree: true });
